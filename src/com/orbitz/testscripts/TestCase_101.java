package com.orbitz.testscripts;
import org.testng.annotations.Test;

import orbitz_project.FlightsInfoPage;
import com.orbitz.utility.Base;

public class TestCase_101 extends Base {

	FlightsInfoPage flights;
	
	@Test
	public void flightBookingExecution() throws Exception {
		
		flights = new FlightsInfoPage(driver);
		flights.flightBooking();
		System.out.println("TestCase_101 Executed");
	}
	
}

