package com.orbitz.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

/**
 * Base class to maintain WebDriver main object and Opening and Closing browser
 * 
 * @author Raviteja.palnati
 *
 */

public class Base {

	public WebDriver driver; /* WebDriver main object */

	@Parameters("browser")

	@BeforeMethod
	public void StartUp(String browser) {
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\ChromeDriver\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();

		} else if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		}

		driver.get("https://www.orbitz.com/");
		System.out.println("Opened application");
	}

	@AfterMethod
	public void tearDown() {
		driver.close();
		System.out.println("Browser Closed");
	}

}
